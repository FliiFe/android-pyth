package pyth.fliife.com.pythinterpreter;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class MainActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    public static String defaultURL = "http://android-pyth.herokuapp.com/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = pref.edit();
        if(pref.getString("baseUrl", defaultURL) == defaultURL){
            editor.putString("baseUrl", defaultURL);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Sending code to the server...", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                sendCode();
            }
        });

    }

    public void sendCode(){
        EditText codeEditText = (EditText) findViewById(R.id.codeEditText);
        EditText inputEditText = (EditText) findViewById(R.id.inputEditText);
        final EditText outputEditText = (EditText) findViewById(R.id.outputEditText);
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        String baseUrl = pref.getString("baseUrl", defaultURL);
        String url = baseUrl + "submit?code=";
        try {
            url += URLEncoder.encode(codeEditText.getText().toString(), "UTF-8");
            url += "&input=";
            url += URLEncoder.encode(inputEditText.getText().toString(), "UTF-8");
            url += "&debug=0";
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        RequestQueue queue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(url, new Response.Listener<String>(){

            @Override
            public void onResponse(String response) {
                outputEditText.setText(response);
            }
        }, new Response.ErrorListener(){

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(stringRequest);
    }

    public static JSONArray search(JSONArray array, String string){
        JSONArray resultArray = new JSONArray();
        try{
            for(int i=0; i<array.length(); i++){
                JSONObject object = array.getJSONObject(i);
                if((object.getString("token").toLowerCase().contains(string.toLowerCase())) || (object.getString("arg").toLowerCase().contains(string.toLowerCase())) || (object.getString("desc").toLowerCase().contains(string.toLowerCase()))){
                    resultArray.put(object);
                }
            }
        }catch(JSONException e){
            e.printStackTrace();
        }
        return resultArray;
    }

    public static void setupUpdater(final View rootView) {
        final SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                updateDoc(swipeRefreshLayout);
            }
        });
    }

    public static void setupSearch(final JSONArray array, final View view){
        EditText searchEditText = (EditText) view.getRootView().findViewById(R.id.searchEditText);

        searchEditText.addTextChangedListener(new TextWatcher(){
            public void afterTextChanged(Editable s) {
                RecyclerView recyclerView = (RecyclerView) view.getRootView().findViewById(R.id.reference_recycler_view);
                recyclerView.setAdapter(new ReferenceAdapter(search(array, s.toString())));
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after){

            }
            public void onTextChanged(CharSequence s, int start, int before, int count){

            }
        });
    }

    public static void updateIfNoDocSaved(){
        //TODO: Implement this (updateIfNoDocSaved)
        SharedPreferences prefs
    }

    public static void updateDoc(final View rootView) {
        RequestQueue queue = Volley.newRequestQueue(rootView.getContext());
        final RecyclerView mRecyclerView = (RecyclerView) rootView.findViewById(R.id.reference_recycler_view);
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(rootView.getContext());
        String baseUrl = pref.getString("baseUrl", defaultURL);
        String url = baseUrl + "rev_doc";
        System.out.println("Downloading JSON object");
        JsonArrayRequest jsObjRequest = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                System.out.println("Got JSON response from server");
                mRecyclerView.setAdapter(new ReferenceAdapter(response));
                setupSearch(response, rootView);
                ((SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout)).setRefreshing(false);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {


            }
        });
        queue.add(jsObjRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class EditorFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public EditorFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static EditorFragment newInstance(int sectionNumber) {
            EditorFragment fragment = new EditorFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            //TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            //textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class ReferenceFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public ReferenceFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static ReferenceFragment newInstance(int sectionNumber) {
            ReferenceFragment fragment = new ReferenceFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_reference, container, false);
            RecyclerView mRecyclerView = (RecyclerView) rootView.findViewById(R.id.reference_recycler_view);

            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            mRecyclerView.setHasFixedSize(true);

            // use a linear layout manager
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(rootView.getContext());
            mRecyclerView.setLayoutManager(mLayoutManager);

            setupUpdater(rootView);
            updateIfNoDocSaved();

            return rootView;
        }
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            if (position == 0) return EditorFragment.newInstance(position + 1);
            if (position == 1) return ReferenceFragment.newInstance(position + 1);
            return null;
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "INTERPRETER";
                case 1:
                    return "REFERENCE";
            }
            return null;
        }
    }
}
