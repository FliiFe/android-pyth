package pyth.fliife.com.pythinterpreter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by fliife on 08/04/16.
 */
public class ReferenceAdapter extends RecyclerView.Adapter<ReferenceAdapter.ViewHolder> {
    private JSONArray doc;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mChar;
        public TextView mDescription;
        public TextView mArg;
        public ViewHolder(View v) {
            super(v);
            mChar = (TextView) v.findViewById(R.id.character_tv);
            mDescription = (TextView) v.findViewById(R.id.desc_tv);
            mArg = (TextView) v.findViewById(R.id.args_tv);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ReferenceAdapter(JSONArray array) {
        this.doc = array;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ReferenceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                          int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_recycler_view, parent, false);
        ReferenceAdapter.ViewHolder vh = new ReferenceAdapter.ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        JSONObject current;
        try {
            current = (JSONObject) doc.get(position);
            holder.mChar.setText(current.getString("token"));
            holder.mDescription.setText(current.getString("desc"));
            holder.mArg.setText(current.getString("arg"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return doc.length();
    }
}